package com.devcamp.task53.exercise;

public interface IRunable {
	void run() ;
	void running() ;
}
