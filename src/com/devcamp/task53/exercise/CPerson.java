package com.devcamp.task53.exercise;

import java.util.ArrayList;

public class CPerson extends CAnimal {
	private int id;
	private int age;
	private String firstName;
	private String lastName;
	private ArrayList <CPet> pets;

	/**
	 * Khởi tạo Constructor không tham số
	 */
	public CPerson() {
		super();
	}

	/**
	 * khởi tạo Cperson với đủ các tham số
	 * @param id
	 * @param age
	 * @param firstName
	 * @param lastName
	 * @param pets
	 */
	public CPerson(int id, int age, String firstName, String lastName, ArrayList<CPet> pets) {
		super();
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.pets = pets;
	}

	
	@Override
	public void animalSound() {
		System.out.println("person speaking.");
	
		/*
		 * Lớp CPerson có 1 phương thức animalSound. 
		Do phương thức này đã có ở lớp CAnimal và lớp CPerson đã kế thừa phương thức này từ lớp CAnimal nên sử dụng Override để chỉ lớp CPerson sẽ ghi đè lên phương thức animalSound

		 */
	}


	@Override
	public String toString() {
		return "CPerson {\"id\":" + this.id + ", age=" + age + ", firstName=" + firstName + ", lastName=" + lastName + ", pets="
				+ this.pets + "}";
	}

    public String getFirstname() {
        return "Firstname = " + this.firstName;
    }

    public String getAge() {
        return "age = " + this.age ;
    }

}
