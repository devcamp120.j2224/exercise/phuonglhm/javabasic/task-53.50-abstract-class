package com.devcamp.task53.exercise;

public abstract class CAnimal {
    public AnimalClass animalclass;

    abstract public void animalSound(); //Lớp trừu tượng phải có ít nhất 1 phương thức trừu tượng được khai báo (animalSound)

    public void eat() {
        System.out.println("Animal eating...");
    }
}
