package com.devcamp.task53.exercise;

import java.util.ArrayList;

public class CMain {

	public static void main(String[] args) {
		CPet dog = new CDog(1,"Lulu");
		
		CPet cat = new CCat(2,"Milo");
		
		ArrayList<CPet> petsList = new ArrayList<>() ;
		petsList.add(dog);
		petsList.add(cat);

		for(CPet pets: petsList) {
			System.out.println(pets);
		}

		CPerson person1 = new CPerson();
		CPerson person2 = new CPerson(1,20,"Le Van","Thang",petsList);

		System.out.println(person1.getFirstname());
		System.out.println(person2.getFirstname());
		System.out.println(person2.getAge());
}
}