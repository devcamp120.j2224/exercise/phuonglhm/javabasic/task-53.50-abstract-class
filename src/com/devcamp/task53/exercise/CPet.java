package com.devcamp.task53.exercise;

public class CPet extends CAnimal {
	protected int age;
	protected String name;
	
	public CPet(int age, String name) {
		this.age = age;
		this.name = name;
	}
	
	@Override
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Pet sound...");

	}
	
	@Override
	public void eat() {
		System.out.println("Pet eating...");
	};
}
