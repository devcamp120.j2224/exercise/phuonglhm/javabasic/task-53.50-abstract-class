package com.devcamp.task53.exercise;

public interface Other {
 void other();
 int other(int param);
 String other(String param);
}
