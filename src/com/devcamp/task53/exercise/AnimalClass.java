package com.devcamp.task53.exercise;

public enum AnimalClass {
	fish, amphibians, reptiles, mammals, birds
}
