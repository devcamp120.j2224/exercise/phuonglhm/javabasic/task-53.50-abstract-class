package com.devcamp.task53.exercise;

public interface IFlyable {
	void fly();
}
